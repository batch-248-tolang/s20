console.log("Hello world");

function greeting(){
	console.log("Are you sleepy boss?");
}

greeting();

// use loop instead of repeating invoke

let countNum = 20;

while(countNum !== 0){
	console.log("This is printed inside the loop" + countNum);
		greeting();
		countNum--;
}

// While Loop

/*
	a while loop takes in an expression/condition
	- expressions are any unit of code that can be evaluated to a value
	- if the condition evaluates to true, the statement inside the code block will be executed
	- a statement is a command that the programmer gives to a computer

	Loop will iterate a certain number of times until an expression/condition is met

	- iteration is the term given to the repetition of statements

	Syntax

	while(expression/condition){
		statement
	}

*/

// while the count is not equal to zero
let count = 5;
// the current value of the count is also printed out
while(count !== 0){
	console.log("While: " + count);
	// decreases the value of "count" by 1 after every iteration to stop the loop when it reaches 0

	// after running script, if a slow response from the browser is experienced or an infinite loop is seen in the console QUICKLY CLOSE the application/browse/tab to avoid this

	count--;//DO NOT FORGET!
}

// do while loop

/*
 a do while loop works a lot like while loop, but unlike while loop. do-while loops guarantee that the code will be executed at least once

 Syntax
 do {
	statement
 }while(expression/condition)

*/

let number = Number(prompt("Give me a number"));

do{
	// the current value of number is printed out
	console.log("Do while: " + number);
	// increases the value of number by 1 after every iteration to stop the loop when it reaches 10 or greater
	// number = number + 1
	number += 1;

	// providing a number of 10 or greater 
}while(number < 10);

//For Loop

/*
    - A for loop is more flexible than while and do-while loops. It consists of three parts:
        1. The "initialization" value that will track the progression of the loop.
        2.  The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
        3. The "finalExpression" indicates how to advance the loop.
    - Syntax
        for (initialization; expression/condition; finalExpression) {
            statement
        }
*/

for (let count = 0; count <= 20; count++){
	console.log("For Loop: " + count);
}

// Mini Activity
for (let count = 0; count <= 20; count++){
	if(count % 2 == 0){
	console.log("Even: " + count);
	}
}

let myString = "Vice Ganda";

console.log(myString.length);

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[9]);

for(let x = 0; x<myString.length;x++){
	console.log(myString[x]);
}

let myName = "Zeref Dragneel";
for(let i=0; i<myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
		){
			console.log("Hi, I'm a vowel!");
	}else{
			console.log(myName[i]);
	}
}

// Continue and Break Statements

/*
	the continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block

	the break statement is used to terminate the current loop once a match has been found
*/

for(let count = 0; count<=20; count++){
	if(count % 2 === 0){
		continue;
	}
	console.log("Continue and Break: " + count);

	if(count >10){
		break;
	}
}

let name = "Mommy Dragneel Yonisia";

for(let i = 0; i < name.length; i++){
	console.log(name[i]);
	if(name[i].toLowerCase()==="a"){
		console.log("Continue tot he next iteration");
		continue;
	}

	if(name[i]=="Y"){
		break
	}
}